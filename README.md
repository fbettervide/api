# Stack Tecnológico WaterClean
# Arquitetura
![Alt text](/public/stack/GeralStackTecnologico.jpg)
# Model Canvas
![Alt text](/public/stack/MapaMental-Planejamento.PNG)
# Diagrama ER
![Alt text](/public/stack/DiagramERWaterClean.png)
# Kanban Piscineiro
![Alt text](/public/stack/KanbanPrestadorDeServico.PNG)
# Kanban Cliente Piscineiro
![Alt text](/public/stack/KanbanCliente.PNG)
# BenchMarketing
![Alt text](/public/stack/BenchMarketing.PNG)
# Prototipagem
https://marvelapp.com/8g588j8/screen/57591358
