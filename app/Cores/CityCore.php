<?php

namespace App\Cores;

use App\Models\City;

class CityCore
{
   /**
    * Listagem de Piscinar 
    */
   public function get($id)
   {
      return City::where('state_id', $id)->get();
   }

    
}
