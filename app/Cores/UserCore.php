<?php

namespace App\Cores;

use App\Models\User;

class UserCore
{

   /**
    *  Registro de Usuários
    */
   public function create($data)
   {
      extract($data);

      $user = new User();

      $user->user_name = $userName;
      $user->password = md5($password);
      $user->status = 1;
      $user->swimming_pool_id = $swimming_pool_id;

      return $user->save();
   }

   public function verify($data)
   {
      $header = [
         'alg' => 'HS256',
         'typ' => 'JWT'
      ];
      $header = json_encode($header);
      $header = base64_encode($header);
      
      $payload = [
         'iss' => 'localhost',
         'name' => $data['userName'],
         'email' => $data['userName']
      ];
      $payload = json_encode($payload);
      $payload = base64_encode($payload);

      $password = 'minha-senha';
      
      $signature = hash_hmac('sha256',"$header.$payload","$password",true);
      $signature = base64_encode($signature);
      
      // echo ;
      // die();
      
      
      // $result = false;
      // $user = User::where([
      //    ['user_name','=', $data['userName']],
      //    ['password','=', md5($data['password'])]
      // ])->first();

      // if(count($user) > 0) {
      //    $result = true;
      // }

      return "$header.$payload.$signature";
      
   }

   public function checkToken($token) {

      $part = explode(".",$token);
      $header = $part[0];
      $payload = $part[1];
      $signature = $part[2];

      $valid = hash_hmac('sha256',"$header.$payload",'minha-senha',true);
      $valid = base64_encode($valid);

      if($signature == $valid){
         return true;
      } else{
         return false;
      }

   }

}
