<?php

namespace App\Cores;

use App\Models\Pool;
use App\Models\Address;
use App\Models\Contact;

class PoolCore
{
   /**
    * Listagem de Piscinar 
    */
   public function listAllPoolSwimming($id)
   {
      // Iterator
      $i = 0;

      // Consulta piscinas vinculadas ao piscineiro
      $pools = Pool::with('address', 'contact')
         ->where('swimming_pool_id', $id)
         ->orderBy('id','desc')
         ->get();

      // Modelagem de informações para retorno   
      foreach ($pools as $pool) {
         // Modela Cidade
         $city = $pool->address->with('city')
            ->where('city_id', $pool->address->city_id)
            ->first()
            ->city;

         // Modela Estados   
         $state = $city->with('state')
            ->where('state_id', $city->state_id)
            ->first()
            ->state;

         // Alimenta objeto de retorno com resultados
         $pools[$i]->address['city'] = $pool->address->with('city')->first()->city;
         $pools[$i]->address['city']['state'] = $state;

         $i++;
      }

      return $pools;
   }

   /**
    *  
    */
   public function create($data)
   {
      
      extract($data);

      // Criar Address
      $addressPool = new Address();
      $addressPool->logradouro = $address['logradouro'];
      $addressPool->number = $address['number'];
      $addressPool->neighborhood = $address['neighborhood'];
      $addressPool->complement = $address['complement'];
      $addressPool->latitude = $address['latitude'];
      $addressPool->longitude = $address['longitude'];
      $addressPool->city_id = $address['city_id'];
      $addressPool->save();

      // Criar Piscina
      $pool = new Pool();
      $pool->name_client = $name_client;
      $pool->image = $image;
      $pool->description = $description;
      $pool->status = $status;
      $pool->address_id = $addressPool->id;
      $pool->swimming_pool_id = $swimming_pool_id;
      $pool->save();

      // Criar Contato
      foreach ($contacts as $contact) {
         $contactPool = new Contact();
         $contactPool->type = $contact['type'];
         $contactPool->value = $contact['value'];
         $contactPool->status = $contact['status'];
         $contactPool->pool_id = $pool->id;
         $contactPool->save();
      }

      return $pool;
   }

   /**
    *  
    */
    public function update($data, $id)
    {
       extract($data);
 
       // Atualizar Piscina
       $pool = Pool::find($id);
       $pool->name_client = $name_client;
       $pool->image = $image;
       $pool->description = $description;
       $pool->status = $status;
       $pool->address_id = $pool->address_id;
       $pool->swimming_pool_id = $pool->swimming_pool_id;
       $pool->save();
 
 
       return $pool;
    }

    /**
     * 
     */
    public function getId($id)
    {
       return Pool::find($id);
    }
}
