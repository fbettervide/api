<?php

namespace App\Cores;

use App\Models\ReportPool;
use App\Models\Address;
use App\Models\Contact;

class ReportPoolCore
{
   /**
    * Listagem de Piscinar 
    */
   public function listAllReportsPool($id)
   {
      // Consulta piscinas vinculadas ao piscineiro
      $pools = ReportPool::where('pool_id', $id)->get();

      return $pools;
   }

   /**
    *  
    */
   public function create($data)
   {
      extract($data);

      // Criar Address
      $addressPool = new Address();
      $addressPool->logradouro = $address['logradouro'];
      $addressPool->number = $address['number'];
      $addressPool->neighborhood = $address['neighborhood'];
      $addressPool->complement = $address['complement'];
      $addressPool->latitude = $address['latitude'];
      $addressPool->longitude = $address['longitude'];
      $addressPool->city_id = $address['city_id'];
      $addressPool->save();

      // Criar Piscina
      $pool = new Pool();
      $pool->name_client = $name_client;
      $pool->description = $description;
      $pool->status = $status;
      $pool->address_id = $addressPool->id;
      $pool->swimming_pool_id = $swimming_pool_id;
      $pool->save();

      // Criar Contato
      foreach ($contacts as $contact) {
         $contactPool = new Contact();
         $contactPool->type = $contact['type'];
         $contactPool->value = $contact['value'];
         $contactPool->status = $contact['status'];
         $contactPool->pool_id = $pool->id;
         $contactPool->save();
      }

      return $pool;
   }

   /**
    *  
    */
    public function update($data, $id)
    {
       extract($data);
 
       // Atualizar Piscina
       $pool = Pool::find($id);
       $pool->name_client = $name_client;
       $pool->description = $description;
       $pool->status = $status;
       $pool->address_id = $pool->address_id;
       $pool->swimming_pool_id = $pool->swimming_pool_id;
       $pool->save();
 
 
       return $pool;
    }

    /**
     * 
     */
    public function getId($id)
    {
       return Pool::find($id);
    }
}
