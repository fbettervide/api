<?php

namespace App\Cores;

use App\Models\ReportPool;
use App\Models\Schedule;
use App\Models\FileReportPool;

class CleaningCore
{
   /**
    * Listagem de Piscinar 
    */
   public function get($id)
   {
      
      $query = ReportPool::join('pool','report_pool.pool_id', '=' ,'pool.id')
         ->join('file_report_pool', 'report_pool.id', '=', 'file_report_pool.report_pool_id')
         ->where('report_pool.id', $id)
         ->get();

      // $query = $cleaningPool->with('pool')->where('id', $id)->get();
      return $query;
   }

   /**
    *  Registro de Limpeza
    */
   public function create($data)
   {
      
      extract($data);

      $cleaningPool = new ReportPool();

      $cleaningPool->description = $description;
      $cleaningPool->check_in = $check_in;
      $cleaningPool->check_out = $check_out;
      $cleaningPool->tot_chlorine = $level_chlorine;
      $cleaningPool->level_ph = $level_ph;
      $cleaningPool->level_alkalinity = $level_alkalinity;
      $cleaningPool->level_conditioner = $level_conditioner;
      $cleaningPool->level_hardness = $level_hardness;
      $cleaningPool->level_phosphate = $level_phosphate;
      $cleaningPool->pool_id = $pool_id;

      $result = false;

      $cleaningPool->save();

      $fileReport = new FileReportPool();
      $fileReport->file = $image;
      $fileReport->report_pool_id = $cleaningPool->id;
      $fileReport->category_file_id = 2;
      $fileReport->save();

      $fileReport = new FileReportPool();
      $fileReport->file = $image2;
      $fileReport->report_pool_id = $cleaningPool->id;
      $fileReport->category_file_id = 3;
      $fileReport->save();

      if($cleaningPool) {
         Schedule::where('pool_id', $pool_id)->update(['status' => 1]);
         $result = true;
      }

      return $result;
   }
}
