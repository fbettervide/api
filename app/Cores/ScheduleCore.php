<?php

namespace App\Cores;

use App\Models\Schedule;
use App\Models\Pool;

class ScheduleCore
{
   /**
    * Listagem de Agenda 
    */
   public function listAll($swimming_pool_id)
   {
      // Consulta piscinas vinculadas ao piscineiro
      $schedules = Schedule::join('pool','schedule.pool_id','pool.id')
                           ->leftJoin('report_pool','pool.id','report_pool.pool_id')
                           ->where('pool.swimming_pool_id', $swimming_pool_id)
                           ->orderBy('schedule.id', 'desc')
                           ->select([
                              'pool.*',
                              'schedule.date_check',
                              'schedule.status',
                              'schedule.time_check',
                              'report_pool.id as reportId'
                              ])
                           ->get();


      return $schedules;
   }

   /**
    * 
    */
    public function create($data)
    {

       extract($data);

       $schedule = new Schedule();
       $schedule->date_check = $date_check;
       $schedule->time_check = $time_check;
       $schedule->status = $status;
       $schedule->pool_id = $pool_id;
       $schedule->save();

       
       return $pool_id;
    }
}
