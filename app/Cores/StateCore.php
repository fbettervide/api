<?php

namespace App\Cores;

use App\Models\State;

class StateCore
{
   /**
    * Listagem de Piscinar 
    */
   public function listAll()
   {
      
      $state = new State();

      return $state->get();
   }

    
}
