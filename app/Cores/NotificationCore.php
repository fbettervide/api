<?php

namespace App\Cores;

use App\Models\Notification;

class NotificationCore
{

   public function getAll($id)
   {
      $notification = new Notification();
      return $notification->where('pool_id', $id)->get();
   }
   
   public function get($id)
   {
      $notification = new Notification();
      return $notification->where('id', $id)->first();
   }

   public function sendNotification($idPool, $message)
   {

        $notificationCore = new NotificationCore();
        $notification =  $notificationCore->get($idPool);

        $content = array(
            "en" => $message
        );

        $userId = $notification->playerId;
        //"4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
        $fields = array(
            'app_id' => "4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
            'data' => array("foo" => "bar"),
            'large_icon' => "ic_launcher_round.png",
            'contents' => $content,
            'include_player_ids' => array($userId)
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ODBkYjE4NzUtMGU1NC00ZWIwLTk1NWQtY2QxZDRlMDRlMTBi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
   }

    
}
