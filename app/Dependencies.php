<?php

// DIC configuration
$container = $app->getContainer();
// database
$container['db'] = function ($container) {
    $capsule = new Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container->get('settings')['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    return $capsule;
};

