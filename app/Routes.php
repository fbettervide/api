<?php 

// $container = $app->getContainer();

$app->group('', function () {

    $this->get('/', 'App\Controllers\HomeController:index')->setName('home.index');

    //Nova Conta
    $this->group('/nova-conta', function () {
        $this->post('/registrar', 'App\Controllers\LoginController:create')->setName('add.users');
    });

    //Auth
    $this->group('/auth', function () {
        $this->post('/token', 'App\Controllers\LoginController:verifyToken')->setName('verigy.token');
        $this->post('', 'App\Controllers\LoginController:auth')->setName('add.auth');
    });

    //Piscinas
    $this->group('/piscinas', function () {
        $this->get('', 'App\Controllers\PoolController:index')->setName('list.pools');
        $this->get('/{id}', 'App\Controllers\PoolController:get')->setName('list.pools');
        $this->post('/add', 'App\Controllers\PoolController:create')->setName('add.pools');
        $this->post('/{id}/edit', 'App\Controllers\PoolController:update')->setName('edit.pools');
        $this->post('/{id}/canceled', 'App\Controllers\PoolController:cancel')->setName('canceled.pools');
        $this->get('/limpezas/{id}', 'App\Controllers\CleaningController:getAll')->setName('list.pools');
    });

    //Limpezas
    $this->group('/limpeza', function () {
        $this->get('/{idPool}', 'App\Controllers\CleaningController:get')->setName('list.cleanning');
        $this->post('/registrar','App\Controllers\CleaningController:create')->setName('created.cleanning');
    });

    // Agenda
    $this->group('/agenda', function () {
        $this->get('', 'App\Controllers\ScheduleController:index')->setName('list.schedule');
        //$this->get('', 'App\Controllers\ScheduleController:testeRetorno')->setName('list.schedule');
        $this->post('/add', 'App\Controllers\ScheduleController:create')->setName('add.schedule');
    });

    $this->get('/states', 'App\Controllers\StateController:index')->setName('state.index');
    $this->get('/cities/{id}', 'App\Controllers\CityController:index')->setName('city.index');

    $this->group('/notification', function () {
        $this->post('/register', 'App\Controllers\NotificationController:register')->setName('notification.register');
        $this->get('', 'App\Controllers\NotificationController:index')->setName('notification.index');
        $this->post('/specific', 'App\Controllers\NotificationController:one')->setName('notification.one');
    });


});