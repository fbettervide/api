<?php
namespace App\Models;

class ReportPool extends Base
{
    protected $table = 'report_pool';

    protected $fillable = [
        'id',
        'description',
        'check_in',
        'check_out',
        'tot_chlorine',
        'level_ph',
        'level_alkalinity',
        'level_conditioner',
        'level_hardness',
        'level_phosphate',
        'pool_id'
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool');
    }

    
}