<?php
namespace App\Models;

class Address extends Base
{
    protected $table = 'address';

    protected $fillable = [
        'id',
        'logradouro',
        'number',
        'neighborhood',
        'complement',
        'latitude',
        'longitude',
        'city_id'
    ];

    public function city() {
        return $this->belongsTo('App\Models\City');
    }

}