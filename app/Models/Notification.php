<?php
namespace App\Models;

class Notification extends Base
{
    protected $table = 'notification';

    protected $fillable = [
        'id',
        'playerId',
        'pool_id',
        'status'
    ];

}