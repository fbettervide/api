<?php
namespace App\Models;

class State extends Base
{
    protected $table = 'state';

    protected $fillable = [
        'id',
        'name',
        'abbreviation'
    ];

}