<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Base extends Model {
    protected $guarded = array('created_at', 'updated_at');
}