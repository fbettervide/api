<?php
namespace App\Models;

class FileReportPool extends Base
{
    protected $table = 'file_report_pool';

    protected $fillable = [
        'id',
        'file',
        'path',
        'report_pool_id',
        'category_file_id'
    ];

}