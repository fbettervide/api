<?php
namespace App\Models;

class Contact extends Base
{
    protected $table = 'contact';

    protected $fillable = [
        'id',
        'type',
        'value',
        'status',
        'pool_id',
        'swimming_pool_id',
    ];

}