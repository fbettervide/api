<?php
namespace App\Models;

class User extends Base
{
    protected $table = 'user';

    protected $fillable = [
        'id',
        'user_name',
        'password',
        'status',
        'swimming_pool_id'
    ];

    public function swimmingPool() {
        return $this->belongsTo('App\Models\SwimmingPool');
    }

}