<?php
namespace App\Models;

class Pool extends Base
{
    protected $table = 'pool';

    protected $fillable = [
        'id',
        'name_client',
        'image',
        'description',
        'status',
        'address_id',
        'swimming_pool_id'
    ];

    public function address() {
        return $this->belongsTo('App\Models\Address');
    }

    public function contact() {
        return $this->hasMany('App\Models\Contact');
    }

    public function schedule() {
        return $this->hasMany('App\Models\Schedule');
    }

    public function reportPool() {
        return $this->belongsTo('App\Models\ReportPool');
    }

    
}