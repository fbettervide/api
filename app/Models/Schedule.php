<?php
namespace App\Models;

class Schedule extends Base
{
    protected $table = 'schedule';

    protected $fillable = [
        'id',
        'date_check',
        'time_check',
        'pool_id'
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool');
    }

    
}