<?php
namespace App\Models;

class SwimmingPool extends Base
{
    protected $table = 'swimming_pool';

    protected $fillable = [
        'id',
        'name',
        'address_id'
    ];


}