<?php
namespace App\Models;

class City extends Base
{
    protected $table = 'city';

    protected $fillable = [
        'id',
        'name'
    ];

    public function state() {
        return $this->belongsTo('App\Models\State');
    }

}