<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;

use App\Cores\ScheduleCore;
use App\Cores\NotificationCore;
use App\Cores\PoolCore;

class ScheduleController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function index(Request $request, Response $response, array $args)
    {
        
        $listSchedule = new ScheduleCore();
        $result = $listSchedule->listAll(1);

        return $response->withJson($result);
    }

    public function create(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();
        
        $addSchedule = new ScheduleCore();
        $idPool = $addSchedule->create($post);

        $notification = new NotificationCore();
        $result = $notification->getAll($idPool);

        if($result) {
            foreach ($result as $pool) {
                $notification = new NotificationCore();
                $notification->sendNotification($pool->id , 'Olá! Sua limpeza foi agendada!');
            }
        }

        return $response->withJson($result);
    }

}
