<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;
use App\Cores\StateCore;

class StateController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $listPool = new StateCore();

        return $response->withJson($listPool->listAll());
    }
    
}
