<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;

use App\Cores\UserCore;

class LoginController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function create(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();

        $createUser = new UserCore();
        return $response->withJson($createUser->create($post));
    }

    public function auth(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();
     
        $authUser = new UserCore();
        
        return $response->withJson($authUser->verify($post));
    }

    public function verifyToken(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();

        $token = $post['token'];
     
        $authUser = new UserCore();
        
        return  $response->withJson($authUser->checkToken($token));
    }

    
}
