<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;

use App\Cores\CleaningCore;
use App\Cores\ReportPoolCore;

class CleaningController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    /**
     * Realizar buscas
     */
    public function getAll(Request $request, Response $response, array $args)
    {
        // Numero id da Piscina
        $idPool = $args['id'];

        $listCleaningPool = new ReportPoolCore();
        $result = $listCleaningPool->listAllReportsPool($idPool);

        return $response->withJson([$result]);
    }

    public function get(Request $request, Response $response, array $args)
    {
        // Numero id do Piscineiro
        $idPool = $args['idPool'];

        $listCleaningPool = new CleaningCore();
        $result = $listCleaningPool->get($idPool);

        return $response->withJson([$result]);
    }

    public function create(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();

        $jpeg = '/jpeg/';
        $png = '/png/';

        $randImg1 = rand(0,1000000);

        if (preg_match($jpeg, $post['image'])) {
            $img = str_replace('data:image/jpeg;base64,', "",  $post['image']);
            $img = str_replace(' ', '+', $img);
            
            $post['image'] = $randImg1 . ".jpeg";
        } else if (preg_match($png, $post['image'])) {
            $img = str_replace('data:image/png;base64,', "",  $post['image']);
            $img = str_replace(' ', '+', $img);
            
            $post['image'] = $randImg1 . ".png";
        }

        $randImg2 = rand(0,1000000);

        if (preg_match($jpeg, $post['image2'])) {
            $img2 = str_replace('data:image/jpeg;base64,', "",  $post['image2']);
            $img2 = str_replace(' ', '+', $img2);
            
            $post['image2'] = $randImg2 . ".jpeg";
        } else if (preg_match($png, $post['image2'])) {
            $img2 = str_replace('data:image/png;base64,', "",  $post['image2']);
            $img2 = str_replace(' ', '+', $img2);
            
            $post['image2'] = $randImg2 . ".png";
        }

        file_put_contents("./public/assets/uploads/" . $post['image'] , base64_decode($img));
        file_put_contents("./public/assets/uploads/" . $post['image2'] , base64_decode($img2));

        $registerCleaningPool = new CleaningCore();
        return $response->withJson($registerCleaningPool->create($post));
    }
}
