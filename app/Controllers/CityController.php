<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;
use App\Cores\CityCore;

class CityController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $listCities = new CityCore();

        return $response->withJson($listCities->get($args['id']));
    }
    
}
