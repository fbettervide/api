<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;
use App\Cores\NotificationCore;

class NotificationController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }


    public function index(Request $request, Response $response, array $args)
    {

        $content = array(
            "en" => 'Olá Seja Bem vindo!'
        );

        //"4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
        $fields = array(
            'app_id' => "4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
            'included_segments' => array('All'),
            'data' => array("foo" => "bar"),
            'large_icon' => "ic_launcher_round.png",
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ODBkYjE4NzUtMGU1NC00ZWIwLTk1NWQtY2QxZDRlMDRlMTBi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);


        $return["allresponses"] = $response;
        $return = json_encode($return);
        print("\n\nJSON received:\n");
        print($return);
        print("\n");
    }

    public function one(Request $request, Response $response, array $args)
    {

        $post = $request->getParsedBody();

        $message = $post['message'];

        $notificationCore = new NotificationCore();
        $notification =  $notificationCore->get($post['idPool']);

        $content = array(
            "en" => $message
        );

        $userId = $notification->playerId;
        //"4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
        $fields = array(
            'app_id' => "4b047fcf-954f-4a92-a7af-2fb87b7d75e1",
            'data' => array("foo" => "bar"),
            'large_icon' => "ic_launcher_round.png",
            'contents' => $content,
            'include_player_ids' => array($userId)
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ODBkYjE4NzUtMGU1NC00ZWIwLTk1NWQtY2QxZDRlMDRlMTBi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);


        $return["allresponses"] = $response;
        $return = json_encode($return);
        print("\n\nJSON received:\n");
        print($return);
        print("\n");
    }
}
