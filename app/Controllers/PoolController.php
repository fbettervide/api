<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;

use App\Cores\PoolCore;

class PoolController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function index(Request $request, Response $response, array $args)
    {
        // Numero id do Piscineiro
        $idPoolSwimming = 1;

        $listPool = new PoolCore();
        $result = $listPool->listAllPoolSwimming($idPoolSwimming);

        return $response->withJson($result);
    }

    public function create(Request $request, Response $response, array $args)
    {
        $post = $request->getParsedBody();

        $jpeg = '/jpeg/';
        $png = '/png/';

        $randImg = rand(0,1000000);

        if (preg_match($jpeg, $post['image'])) {
            $img = str_replace('data:image/jpeg;base64,', "",  $post['image']);
            $img = str_replace(' ', '+', $img);
            
            $post['image'] = $randImg . ".jpeg";
        } else if (preg_match($png, $post['image'])) {
            $img = str_replace('data:image/png;base64,', "",  $post['image']);
            $img = str_replace(' ', '+', $img);
            
            $post['image'] = $randImg . ".png";
        }

        file_put_contents("./public/assets/uploads/" . $post['image'] , base64_decode($img));

        $createPool = new PoolCore();
        
        return $response->withJson($createPool->create($post));
    }

    public function update(Request $request, Response $response, array $args)
    {
        $idPool = $args['id'];
       
        $post = $request->getParsedBody();

        $updatePool = new PoolCore();
        return $response->withJson($updatePool->update($post, $idPool));
    }

    public function get(Request $request, Response $response, array $args)
    {
        $idPool = $args['id'];
        $pool = new PoolCore();
        $retorno = $pool->getId($idPool);

        return $response->withJson([$retorno]);
    }
    
}
