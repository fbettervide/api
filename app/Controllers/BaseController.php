<?php
namespace App\Controllers;

use Interop\Container\ContainerInterface;

class BaseController
{
    /*
     |--------------------------------------------------------------------------
     | Base Controller
     |--------------------------------------------------------------------------
     |
     | Este controlador controla as configurações de DB,Mailer,Logs,Views.
     |
     */
    protected $container;
    protected $db;

    public function __construct(ContainerInterface $c)
    {
        $this->container = $c;
        $this->db = $c->get('db');

    }
}