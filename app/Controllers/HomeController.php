<?php

namespace App\Controllers;

use Interop\Container\ContainerInterface;

use Slim\Http\Response;
use Slim\Http\Request;

class HomeController extends BaseController
{

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $array = [1,2,3,4];

        return $response->withJson($array);
    }
    
}
