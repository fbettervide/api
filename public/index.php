<?php

// header('Access-Control-Allow-Origin: *');  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

include __DIR__.'/../vendor/autoload.php';

$settings = [
    'settings' => [
        'displayErrorDetails' => true,
        // Database settings
        'db' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'water_clean',
            'username'  => 'root',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
        ]
    ]
];

$app = new \Slim\App($settings);

require __DIR__ . '/../app/Dependencies.php';

include __DIR__.'/../app/Routes.php';


// Run app
$app->run();